function allLetter(word){
  var letters = /^[A-Za-z]+$/;
  if(word.value.match(letters)){
    return true;
  }else{
    return false;
  }
}
function allNumeric(number){
  var numbers = /^[0-9]+$/;
  if(number.value.match(numbers)){
    return true;
  }else{
    return false;
  }
}
function userid_validation(uid,reqLength,error_field){
  var uid_len = uid.value.length;
  if(uid_len == 0 ){
    error_field.innerHTML="ID Number should not be empty!";
    uid.focus();
    return false;
  }
  if(uid_len != reqLength){
    error_field.innerHTML="ID Number should be "+reqLength+" digits!";
    return false;
  }
  if(!(allNumeric(uid))){
    error_field.innerHTML="ID Number must contain numbers only!";
    return false;
  }
  return true;
}
function pass_validation(pass,cpass,error_field){
  var pass_len = pass.value.length;
  if(pass_len == 0){
    error_field.innerHTML="Password should not be empty!";
    pass.focus();
    return false;
  }
  if(pass.value != cpass.value){
    error_field.innerHTML="Passwords do not match!";
    cpass.focus();
    return false;
  }
  return true;
}
function name_validation(name,fieldname,error_field){
  var name_len = name.value.length;
  if(name_len == 0){
    error_field.innerHTML=fieldname + " should not be empty!";
    name.focus();
    return false;
  }
  if(!(allLetter(name))){
    error_field.innerHTML=fieldname + " should contain english letters only!";
    name.focus();
    return false;
  }
  return true;
}
function email_validation(email,error_field){
  var email_len = email.value.length;
  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if( email_len == 0 ){
    error_field.innerHTML="Email should not be empty!";
    email.focus();
    return false;
  }
  if(email.value.match(mailformat)){
    return true;
  }else{
    error_field.innerHTML="You have entered an invalid email address!";
    email.focus();
    return false;
  }
}
function formValidation(){
  var userid = document.getElementById("UserID");
  var fname = document.getElementById("fName");
  var lname = document.getElementById("lName");
  var email = document.getElementById("email");
  var pass = document.getElementById("Password");
  var cpass = document.getElementById("ConfirmPassword");
  var error_field = document.getElementById("error-field");
  error_field.innerHTML="";
  if(userid_validation(userid,9,error_field)){
    if(name_validation(fname,"First name",error_field)){
      if(name_validation(lname,"Last name",error_field)){
        if(email_validation(email,error_field)){
          if(pass_validation(pass,cpass,error_field)){
              return true;
          }
        }
      }
    }
  }
  return false;
}

