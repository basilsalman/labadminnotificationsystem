<?php
require_once 'config.php';
require_once 'libs/PHPMailer/PHPMailerAutoload.php';
include "open_projects.php";
include "projects_awaiting_approval.php";
include "projects_awaiting_first_meeting.php";
include "projects_awaiting_grading.php";


function dbConnect(){
  global $DB;
  $conn = new mysqli($DB['host'], $DB['user'], $DB['pass'], $DB['dbname'],$DB['port'],$DB['sock']);
  // Check connection
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }
  return $conn;
}
 function mydbConnect(){
  global $MYDB;
  $conn = new mysqli($MYDB['host'], $MYDB['user'], $MYDB['pass'], $MYDB['dbname'],$MYDB['port'],$MYDB['sock']);
  // Check connection
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }
  return $conn;


}

function configPHPMailer(){
  global $SMTP;
  $mailer = new PHPMailer();
  $mailer->IsSmtp();
  $mailer->IsHTML(true);
  $mailer->Host = $SMTP['host'];
  $mailer->Username = $SMTP['username'];
  $mailer->Password = $SMTP['password'];
  $mailer->SMTPAuth = $SMTP['SMTPAuth'];
  $mailer->Port = $SMTP['port'];
  $mailer->SMTPSecure = $SMTP['SMTPSecure'];
  $mailer->FromName = "Labadmin Notification System";
  return $mailer;
}

function getUsers(&$result){
  $conn = mydbConnect();
  $qry = "Select Email,UserId,FirstName from Users Order by UserId ASC";
  $result = $conn->query($qry);

}

function SanitizeForSQL($str)
{
  if( function_exists( "mysql_real_escape_string" ) )
  {
    $ret_str = mysqli_real_escape_string( $str );
  }
  else
  {
    $ret_str = addslashes( $str );
  }
  return $ret_str;
}





function getAlertPeriodicity($Alertid,&$result){
  $conn = mydbConnect();
  $id = SanitizeForSQL($Alertid);
  $qry = "SELECT description FROM `Alerts`,`Periods`
WHERE
`Alerts`.`PeriodId` = `Periods`.`PeriodId`
AND
`Alerts`.`AlertId`=$id";
  $result = $conn->query($qry);
  if(!$result || mysqli_num_rows($result) <= 0)
  {
    return false;
  }
  return true;
}

function prepareEmailHeader($user,&$mailer,$AlertTypeId){
  $result = array();
  $AlertId="" . $user['UserId'] . "" .$AlertTypeId ;
  if(!(getAlertPeriodicity($AlertId, $result)))
    return false;
  $period=$result->fetch_array();
  $mailer->Body="<html>
  <body>
    <h3>
      Hello ". $user['FirstName']. ", <br>
      Here is your " .$period['description']." update:
    </h3>";
  return true;
}

function prepareEmailFooter($user,&$mailer){
$mailer->Body.=" <h5>for more information please visit our website.</h5>
  </body>
</html>";
}



function prepareOpenProjectsEmail($user,&$mailer){
  if(!(prepareEmailHeader($user,$mailer,"1")))
    return false;
  if(!(prepareOpenProjectsTable($user,$mailer)))
    return false;
  print_r($user);
  prepareEmailFooter($user,$mailer);
  print_r($mailer->Body);
  return true;
}
?>
