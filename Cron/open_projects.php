
<?php


function getOpenProjects($userid,&$result){
  $conn = dbConnect();
  $id=SanitizeForSQL($userid);
  $qry = "SELECT project_name_eng,`semester_display_name`,`users`.first_name,`users`.last_name,`users`.`tz`, labs.lab_name
FROM projects,`users`,`labs`,`semester`
WHERE (project_status=20 OR project_status=100 OR project_status=10) AND projects.is_active=1
AND is_canceled=0 AND (projects.created_by=tz OR main_super_tz=tz)
AND projects.id_labs = labs.id_labs
AND projects.id_semester = semester.id_semester
AND projects.main_super_tz = users.tz
AND users.tz = $userid";
  $result = $conn->query($qry);
  if(!$result || mysqli_num_rows($result) <= 0)
  {
    return false;
  }
  return true;
}


function prepareOpenProjectsTable($user,&$mailer){
  $mailer->Body.="<h3>Opened projects...</h3>";
  
  $mailer->Body.=' <table border="1" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Project Name</th>
                  <th>Laboratory</th>
                  <th>Semester</th>
                  <th>Supervisor</th>

                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Project Name</th>
                  <th>Laboratory</th>
                  <th>Semester</th>
                  <th>Supervisor</th>
                  
                </tr>
              </tfoot>
              <tbody>';
  $projects = array();
  if(!(getOpenProjects($user['UserId'],$projects)))
  {
    return false;
  }
  while($project = $projects->fetch_array())
  {
    $mailer->Body .= "<tr>
                      <td>" . $project["project_name_eng"]. "</td>
                      <td>" . $project["lab_name"]. "</td>
                      <td>" . $project["semester_display_name"] . "</td>
                      <td>" . $project["first_name"] . " " . $project["last_name"] . "</td>
                      </tr>";
  }
  $mailer->Body.="</tbody>
            </table>";
  return true;
}

?>