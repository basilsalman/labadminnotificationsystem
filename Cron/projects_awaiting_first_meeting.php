<?php

function getFirstMeetingMissingProjects($userid,&$result){
  $conn = dbConnect();
  $id=SanitizeForSQL($userid);
  $qry = "SELECT `project_name_eng`,`lab_name`,`semester_display_name`,`users`.first_name,`users`.last_name,`users`.tz,projects.created_on
FROM `projects`,`labs`,`users`,`semester`
WHERE `project_status` = 12 
AND projects.`is_active` = 1
AND projects.id_labs = labs.id_labs
AND is_canceled=0 
AND projects.id_semester = semester.id_semester
AND projects.main_super_tz = users.tz
AND users.tz = $userid";
  $result = $conn->query($qry);
  if(!$result || mysqli_num_rows($result) <= 0)
  {
    return false;
  }
  return true;
}


function prepareFirstMeetingMissingProjectsTable($user,&$mailer){
  $mailer->Body.="<h3>Projects that are waiting for students first meeting...</h3>";
  
  $mailer->Body.=' <table border="1" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Project Name</th>
                  <th>Laboratory</th>
                  <th>Semester</th>
                  <th>Project Initialization</th>

                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Project Name</th>
                  <th>Laboratory</th>
                  <th>Semester</th>
                  <th>Project Initialization</th>
                  
                </tr>
              </tfoot>
              <tbody>';
  $projects = array();
  if(!(getFirstMeetingMissingProjects($user['UserId'],$projects)))
  {
    return false;
  }
  while($project = $projects->fetch_array())
  {
    $mailer->Body .= "<tr>
                      <td>" . $project["project_name_eng"]. "</td>
                      <td>" . $project["lab_name"]. "</td>
                      <td>" . $project["semester_display_name"] . "</td>
                      <td>" . date("Y-m-d\TH:i:s\Z", $project["created_on"]) . "</td>
                      </tr>";
  }
  $mailer->Body.="</tbody>
            </table>";
  return true;
}

?>