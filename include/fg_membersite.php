<?PHP
/*
    Registration/Login script from HTML Form Guide
    V1.0

    This program is free software published under the
    terms of the GNU Lesser General Public License.
    http://www.gnu.org/copyleft/lesser.html
    

This program is distributed in the hope that it will
be useful - WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.

For updates, please visit:
http://www.html-form-guide.com/php-form/php-registration-form.html
http://www.html-form-guide.com/php-form/php-login-form.html

*/
require_once("class.phpmailer.php");

class FGMembersite
{
    var $admin_email;
    var $from_address;
    
    var $username;
    var $pwd;
    var $database;
    var $tablename;
    var $connection;
    var $rand_key;
    var $port; 
    var $sock;

    
    var $error_message;
    var $error_user;
    //-----Initialization -------
    function FGMembersite()
    {
        $this->sitename = 'YourWebsiteName.com';
        $this->rand_key = '0iQx5oBk66oVZep';
    }
    
    function InitDB($host,$uname,$pwd,$database,$tablename,$port,$sock)
    {
        $this->db_host  = $host;
        $this->username = $uname;
        $this->pwd  = $pwd;
        $this->database  = $database;
        $this->tablename = $tablename;
	$this->port = $port;
	$this->sock = $sock;
        
    }
    function SetAdminEmail($email)
    {
        $this->admin_email = $email;
    }
    
    function SetWebsiteName($sitename)
    {
        $this->sitename = $sitename;
    }
    
    function SetRandomKey($key)
    {
        $this->rand_key = $key;
    }
    
    //-------Main Operations ----------------------
    function RegisterUser()
    {
        if(!isset($_POST['Register']))
        {
           return false;
        }
        $formvars = array();
        
        $this->CollectRegistrationSubmission($formvars);
        if(!$this->SaveToDatabase($formvars))
        {
            return false;
        }
        /*if(!$this->SendUserConfirmationEmail($formvars))
        {
            return false;
        }*/ //TODO

        //$this->SendAdminIntimationEmail($formvars);
        
        return true;
    }
    
    function SaveSettings()
    {
       if(!isset($_POST['SaveSettings']))
       {
          return false;
       }
       $formvars= array();
       $this->CollectSettingsSubmission($formvars);
       if(!$this->SaveSettingsToDatabase($formvars))
       {
          return false;
       }
       return true;
    }
    function ConfirmUser()
    {
        if(empty($_GET['code'])||strlen($_GET['code'])<=10)
        {
            $this->HandleError("Please provide the confirm code");
            return false;
        }
        $user_rec = array();
        if(!$this->UpdateDBRecForConfirmation($user_rec))
        {
            return false;
        }
        
        $this->SendUserWelcomeEmail($user_rec);
        
        $this->SendAdminIntimationOnRegComplete($user_rec);
        
        return true;
    }
    function isAdmin(){
      if(!isset($_SESSION)){return false; }
      return isset($_SESSION['user_type'])?$_SESSION['user_type']==0:false;
    }
    function isManager(){
      if(!isset($_SESSION)){ return false; }
      return isset($_SESSION['user_type'])?$_SESSION['user_type']==2:false;
    }    
    function Login()
    {
        if(empty($_POST['UserID']))
        {
            $this->HandleError("ID is empty!");
            return false;
        }
        
        if(empty($_POST['Password']))
        {
            $this->HandleError("Password is empty!");
            return false;
        }
        
        $userid = trim($_POST['UserID']);
        $password = trim($_POST['Password']);
        
        if(!isset($_SESSION)){ session_start(); }
        if(!$this->CheckLoginInDB($userid,$password))
        {
            return false;
        }
        
        $_SESSION[$this->GetLoginSessionVar()] = $userid;
        
        return true;
    }
    
    function CheckLogin()
    {
         if(!isset($_SESSION)){ session_start(); }

         $sessionvar = $this->GetLoginSessionVar();
         
         if(empty($_SESSION[$sessionvar]))
         {
            return false;
         }
         return true;
    }
    
    function UserId()
    {
      return isset($_SESSION['UserID'])?$_SESSION['UserID']:'';
    }
    function UserFullName()
    {
        return isset($_SESSION['name_of_user'])?$_SESSION['name_of_user']:'';
    }
    function UserIsAdmin()
    {
      return isset($_SESSION['user_type'])?($_SESSION['user_type']==0):false;
    } 
    function UserEmail()
    {
        return isset($_SESSION['email_of_user'])?$_SESSION['email_of_user']:'';
/*	$sessionvar = $this->GetLoginSessionVar();
	return isset($_SESSION[$sessionvar])?$_SESSION[$sessionvar]:'';*/
    }
    
    function LogOut()
    {
        session_start();
        
        $sessionvar = $this->GetLoginSessionVar();
        
        $_SESSION[$sessionvar]=NULL;
        
        unset($_SESSION[$sessionvar]);
    }
    
    function EmailResetPasswordLink()
    {
        if(empty($_POST['email']))
        {
            $this->HandleError("Email is empty!");
            return false;
        }
        $user_rec = array();
        if(false === $this->GetUserFromEmail($_POST['email'], $user_rec))
        {
            return false;
        }
        if(false === $this->SendResetPasswordLink($user_rec))
        {
            return false;
        }
        return true;
    }
    
    function ResetPassword()
    {
        if(empty($_GET['email']))
        {
            $this->HandleError("Email is empty!");
            return false;
        }
        if(empty($_GET['code']))
        {
            $this->HandleError("reset code is empty!");
            return false;
        }
        $email = trim($_GET['email']);
        $code = trim($_GET['code']);
        
        if($this->GetResetPasswordCode($email) != $code)
        {
            $this->HandleError("Bad reset code!");
            return false;
        }
        
        $user_rec = array();
        if(!$this->GetUserFromEmail($email,$user_rec))
        {
            return false;
        }
        
        $new_password = $this->ResetUserPasswordInDB($user_rec);
        if(false === $new_password || empty($new_password))
        {
            $this->HandleError("Error updating new password");
            return false;
        }
        
        if(false == $this->SendNewPassword($user_rec,$new_password))
        {
            $this->HandleError("Error sending new password");
            return false;
        }
        return true;
    }
    
    function ChangePassword()
    {
        if(!$this->CheckLogin())
        {
            $this->HandleError("Not logged in!");
            return false;
        }
        
        if(empty($_POST['oldpwd']))
        {
            $this->HandleError("Old password is empty!");
            return false;
        }
        if(empty($_POST['newpwd']))
        {
            $this->HandleError("New password is empty!");
            return false;
        }
        
        $user_rec = array();
        if(!$this->GetUserFromEmail($this->UserEmail(),$user_rec))
        {
            return false;
        }
        
        $pwd = trim($_POST['oldpwd']);
        
        if($user_rec['password'] != md5($pwd))
        {
            $this->HandleError("The old password does not match!");
            return false;
        }
        $newpwd = trim($_POST['newpwd']);
        
        if(!$this->ChangePasswordInDB($user_rec, $newpwd))
        {
            return false;
        }
        return true;
    }
    
    //-------Public Helper functions -------------
    function GetSelfScript()
    {
        return htmlentities($_SERVER['PHP_SELF']);
    }    
    
    function SafeDisplay($value_name)
    {
        if(empty($_POST[$value_name]))
        {
            return'';
        }
        return htmlentities($_POST[$value_name]);
    }
    
    function RedirectToURL($url)
    {
        header("Location: $url");
        exit;
    }
    
    function GetErrorMessage()
    {
        if(empty($this->error_message))
        {
            return '';
        }
        $errormsg = nl2br(htmlentities($this->error_message));
        return $errormsg;
    }
    function PrintError()
    {
      echo $this->error_user;
    } 
    //-------Private Helper functions-----------
    
    function HandleError($err)
    {
        $this->error_user = $err;
        $this->error_message .= $err."\r\n";
    }
    
    function HandleDBError($err)
    {
        $this->HandleError($err."\r\n mysqlerror:".mysql_error());
    }
    
    function GetFromAddress()
    {
        if(!empty($this->from_address))
        {
            return $this->from_address;
        }

        $host = $_SERVER['SERVER_NAME'];

        $from ="nobody@$host";
        return $from;
    } 
    
    function GetLoginSessionVar()
    {
        $retvar = md5($this->rand_key);
        $retvar = 'usr_'.substr($retvar,0,10);
        return $retvar;
    }
    
    function CheckLoginInDB($userid,$password)
    {
	if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }          
        $userid = $this->SanitizeForSQL($userid);
        $pwdmd5 = md5($password);
#        $qry = "Select name, email from $this->tablename where userid='$userid' and password='$pwdmd5' and confirmcode='y'";
        $qry = "Select FirstName,LastName,Email,UserTypeId from $this->tablename where userid='$userid' and password='$pwdmd5'";
        $result = ($this->connection)->query($qry);
        
		if(!$result || mysqli_num_rows($result) <= 0)
        {
            $this->HandleError("Error logging in. The username or password does not match");
            return false;
        }
        
        $row = mysqli_fetch_assoc($result);
        
        $_SESSION['UserID'] = $userid;
        $_SESSION['name_of_user']  = $row['FirstName'];
	$_SESSION['last_name_of_user'] = $row['LastName'];
        $_SESSION['email_of_user'] = $row['Email'];
        $_SESSION['user_type'] = $row['UserTypeId'];
        return true;
    }
    function GetAlertTypes(&$result){
       if(!$this->DBLogin())
       {
           $this->HandleError("Database login failed!");
            return false;
       }
    $qry = "Select AlertTypeId, Description from AlertTypes Order by AlertTypeId ASC";
    $result = ($this->connection)->query($qry);
    return true;
    } 
    function AlertTypes()
    {
       if(!$this->DBLogin())
       {
           $this->HandleError("Database login failed!");
            return false;
       }
    $qry2 = "Select PeriodId, Description from Periods";
    $result = array();
    $this->GetAlertTypes($result);
    while($row = $result->fetch_array())
    {
    $result2 = ($this->connection)->query($qry2);
    echo '<br><h5>' .$row["Description"].':</h5>
	<h6>';
        while($Period = $result2->fetch_array())
        {
        echo '<label class="radio-inline">
         <input type="radio" name="'.$row["AlertTypeId"].'" value="'.$Period["PeriodId"].'" >&nbsp;'.$Period["Description"].'&nbsp;
        </label>&nbsp;';
	}
      echo '</h6>';
    }    
    }
  
    function UpdateDBRecForConfirmation(&$user_rec)
    {
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }   
        $confirmcode = $this->SanitizeForSQL($_GET['code']);
        
        $result = mysql_query("Select name, email from $this->tablename where confirmcode='$confirmcode'",$this->connection);   
        if(!$result || mysql_num_rows($result) <= 0)
        {
            $this->HandleError("Wrong confirm code.");
            return false;
        }
        $row = mysql_fetch_assoc($result);
        $user_rec['name'] = $row['name'];
        $user_rec['email']= $row['email'];
        
        $qry = "Update $this->tablename Set confirmcode='y' Where  confirmcode='$confirmcode'";
        
        if(!mysql_query( $qry ,$this->connection))
        {
            $this->HandleDBError("Error inserting data to the table\nquery:$qry");
            return false;
        }      
        return true;
    }
    
    function ResetUserPasswordInDB($user_rec)
    {
        $new_password = substr(md5(uniqid()),0,10);
        
        if(false == $this->ChangePasswordInDB($user_rec,$new_password))
        {
            return false;
        }
        return $new_password;
    }
    
    function ChangePasswordInDB($user_rec, $newpwd)
    {
        $newpwd = $this->SanitizeForSQL($newpwd);
        
        $qry = "Update $this->tablename Set password='".md5($newpwd)."' Where  id_user=".$user_rec['id_user']."";
        
        if(!mysql_query( $qry ,$this->connection))
        {
            $this->HandleDBError("Error updating the password \nquery:$qry");
            return false;
        }     
        return true;
    }
    
    function GetUserFromEmail($email,&$user_rec)
    {
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }   
        $email = $this->SanitizeForSQL($email);
        
        $result = mysql_query("Select * from $this->tablename where email='$email'",$this->connection);  

        if(!$result || mysql_num_rows($result) <= 0)
        {
            $this->HandleError("There is no user with email: $email");
            return false;
        }
        $user_rec = mysql_fetch_assoc($result);

        
        return true;
    }
    
    function SendUserWelcomeEmail(&$user_rec)
    {
        $mailer = new PHPMailer();
        
        $mailer->CharSet = 'utf-8';
        
        $mailer->AddAddress($user_rec['email'],$user_rec['name']);
        
        $mailer->Subject = "Welcome to ".$this->sitename;

        $mailer->From = $this->GetFromAddress();        
        
        $mailer->Body ="Hello ".$user_rec['name']."\r\n\r\n".
        "Welcome! Your registration  with ".$this->sitename." is completed.\r\n".
        "\r\n".
        "Regards,\r\n".
        "Webmaster\r\n".
        $this->sitename;

        if(!$mailer->Send())
        {
            $this->HandleError("Failed sending user welcome email.");
            return false;
        }
        return true;
    }
    
    function SendAdminIntimationOnRegComplete(&$user_rec)
    {
        if(empty($this->admin_email))
        {
            return false;
        }
        $mailer = new PHPMailer();
        
        $mailer->CharSet = 'utf-8';
        
        $mailer->AddAddress($this->admin_email);
        
        $mailer->Subject = "Registration Completed: ".$user_rec['name'];

        $mailer->From = $this->GetFromAddress();         
        
        $mailer->Body ="A new user registered at ".$this->sitename."\r\n".
        "Name: ".$user_rec['name']."\r\n".
        "Email address: ".$user_rec['email']."\r\n";
        
        if(!$mailer->Send())
        {
            return false;
        }
        return true;
    }
    
    function GetResetPasswordCode($email)
    {
       return substr(md5($email.$this->sitename.$this->rand_key),0,10);
    }
    
    function SendResetPasswordLink($user_rec)
    {
        $email = $user_rec['email'];
        
        $mailer = new PHPMailer();
        
        $mailer->CharSet = 'utf-8';
        
        $mailer->AddAddress($email,$user_rec['name']);
        
        $mailer->Subject = "Your reset password request at ".$this->sitename;

        $mailer->From = $this->GetFromAddress();
        
        $link = $this->GetAbsoluteURLFolder().
                '/resetpwd.php?email='.
                urlencode($email).'&code='.
                urlencode($this->GetResetPasswordCode($email));

        $mailer->Body ="Hello ".$user_rec['name']."\r\n\r\n".
        "There was a request to reset your password at ".$this->sitename."\r\n".
        "Please click the link below to complete the request: \r\n".$link."\r\n".
        "Regards,\r\n".
        "Webmaster\r\n".
        $this->sitename;
        
        if(!$mailer->Send())
        {
            return false;
        }
        return true;
    }
    
    function SendNewPassword($user_rec, $new_password)
    {
        $email = $user_rec['email'];
        
        $mailer = new PHPMailer();
        
        $mailer->CharSet = 'utf-8';
        
        $mailer->AddAddress($email,$user_rec['name']);
        
        $mailer->Subject = "Your new password for ".$this->sitename;

        $mailer->From = $this->GetFromAddress();
        
        $mailer->Body ="Hello ".$user_rec['name']."\r\n\r\n".
        "Your password is reset successfully. ".
        "Here is your updated login:\r\n".
        "username:".$user_rec['username']."\r\n".
        "password:$new_password\r\n".
        "\r\n".
        "Login here: ".$this->GetAbsoluteURLFolder()."/login.php\r\n".
        "\r\n".
        "Regards,\r\n".
        "Webmaster\r\n".
        $this->sitename;
        
        if(!$mailer->Send())
        {
            return false;
        }
        return true;
    }    
    
    function CollectRegistrationSubmission(&$formvars)
    {	
		$formvars['UserID'] = $this->Sanitize($_POST['UserID']);
		$formvars['fName'] = $this->Sanitize($_POST['fName']);
        $formvars['lName'] = $this->Sanitize($_POST['lName']);
        $formvars['email'] = $this->Sanitize($_POST['email']);
        $formvars['Password'] = $this->Sanitize($_POST['Password']);
    }
    function CollectSettingsSubmission(&$formvars)
    {
    $result = array();
    $this->GetAlertTypes($result);
    while($row = $result->fetch_array())
      {
	    $formvars[$row['AlertTypeId']] = $this->Sanitize($_POST[$row['AlertTypeId']]);
      }
    }
    function SendUserConfirmationEmail(&$formvars)
    {
        $mailer = new PHPMailer();
        
        $mailer->CharSet = 'utf-8';
        
        $mailer->AddAddress($formvars['email'],$formvars['name']);
        
        $mailer->Subject = "Your registration with ".$this->sitename;

        $mailer->From = $this->GetFromAddress();        
        
        $confirmcode = $formvars['confirmcode'];
        
        $confirm_url = $this->GetAbsoluteURLFolder().'/confirmreg.php?code='.$confirmcode;
        
        $mailer->Body ="Hello ".$formvars['name']."\r\n\r\n".
        "Thanks for your registration with ".$this->sitename."\r\n".
        "Please click the link below to confirm your registration.\r\n".
        "$confirm_url\r\n".
        "\r\n".
        "Regards,\r\n".
        "Webmaster\r\n".
        $this->sitename;

        if(!$mailer->Send())
        {
            $this->HandleError("Failed sending registration confirmation email.");
            return false;
        }
        return true;
    }
    function GetAbsoluteURLFolder()
    {
        $scriptFolder = (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on')) ? 'https://' : 'http://';
        $scriptFolder .= $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']);
        return $scriptFolder;
    }
    
    function SendAdminIntimationEmail(&$formvars)
    {
        if(empty($this->admin_email))
        {
            return false;
        }
        $mailer = new PHPMailer();
        
        $mailer->CharSet = 'utf-8';
        
        $mailer->AddAddress($this->admin_email);
        
        $mailer->Subject = "New registration: ".$formvars['name'];

        $mailer->From = $this->GetFromAddress();         
        
        $mailer->Body ="A new user registered at ".$this->sitename."\r\n".
        "Name: ".$formvars['name']."\r\n".
        "Email address: ".$formvars['email']."\r\n".
        "UserName: ".$formvars['username'];
        
        if(!$mailer->Send())
        {
            return false;
        }
        return true;
    }
    
    function SaveToDatabase(&$formvars)
    {
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }
        
        if(!$this->Ensuretable())
        {
            return false;
        }
        if(!$this->InputValidation($formvars))
        {
           return false;
        }
        if(!$this->IsFieldUnique($formvars,'email'))
        {
            $this->HandleError("This email is already registered");
            return false;
        }
        if(!$this->IsFieldUnique($formvars,'UserID'))
        {
            $this->HandleError("This ID Number is already registered, Please contact the system Administrator");
            return false;
        }	
        if(!$this->InsertUserIntoDB($formvars))
        {
            $this->HandleError("Inserting to Database failed!");
            return false;
        }
        return true;
    }
    
    function SaveSettingsToDatabase(&$formvars)
    {
      if(!$this->DBLogin())
      {
          $this->HandleError("Database login failed!");
          return false;
      }
	  if(!$this->InsertSettingIntoDB($formvars))
	  {
		  $this->HandleError("Inserting to Database failed!");
		  return false;
	  }
      return true;
    }
    function allNumeric($number)
    {
      $numbers = '/^[0-9]+$/';
      if(preg_match($numbers,$number))
      {
        return true;
      }
      return false;
    }
    function allLetter($word)
    {
      $letters = '/^[A-Za-z]+$/';
      if(preg_match($letters,$word))
      {
        return true;
      }
      return false;
    }
    function UseridValidation($userid, $len)
    {
      $userid_len = strlen($userid);
      if($userid_len ==0 )
      {
        $this->HandleError("ID Number should not be empty!");
        return false;
      }
      if($userid_len != $len)
      {
        $this->HandleError("ID Number must be " .$len." digits!");
        return false;
      } 
      if(!($this->allNumeric($userid)))
      {
        $this->HandleError("ID Number must contain numbers only!");
        return false;
      }
      return true;
    }
    function passValidation($pass)
    {
      $pass_len = strlen($pass);
      if($pass_len == 0)
      {
        $this->HandleError("Password should not be empty!");
        return false;
      }
      return true;
    }
    function nameValidation($name, $fieldname)
    {
      $name_len = strlen($name);
      if($name_len == 0)
      {
        $this->HandleError($fieldname." should not be empty!");
        return false;
      }
      if(!($this->allLetter($name)))
      {
        $this->HandleError($fieldname." should contain english letters only!");
        return false;
      }
      return true;
    }
    function emailValidation($email)
    {
      $email_len = strlen($email);
      $mailformat = '/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/';
      if( $email_len == 0){
        $this->HandleError("Email should not be empty!");
        return false;
      }
      if(!(preg_match($mailformat, $email)))
      {
        $this->HandleError("You have entered an invalid email address!");
        return false;
      }
      return true;
    }
    function InputValidation(&$formvars)
    {
      $userid = $formvars['UserID'];
      $fname = $formvars['fName'];
      $lname = $formvars['lName'];
      $email = $formvars['email'];
      $pass = $formvars['Password'];

      if(!($this->UseridValidation($userid,9))){
        return false;
      }
      if(!($this->passValidation($pass))){
        return false;
      }

      if(!($this->nameValidation($fname,'First name'))){
        return false;
      }
      if(!($this->nameValidation($lname,'Last name'))){
        return false;
      }
      if(!($this->emailValidation($email))){
        return false;
      } 
      return true;
    } 
    function IsFieldUnique($formvars,$fieldname)
    {
        $field_val = $this->SanitizeForSQL($formvars[$fieldname]);
        $qry = "select UserId from $this->tablename where $fieldname='".$field_val."'";
        $result = ($this->connection)->query($qry);
        if($result && mysqli_num_rows($result) > 0)
        {
            return false;
        }
        return true;
    }
    
    function DBLogin()
    {
        $this->connection = new mysqli($this->db_host,$this->username,$this->pwd,$this->database,$this->port,$this->sock);
        if(!$this->connection)
        {   
            $this->HandleDBError("Database Login failed! Please make sure that the DB login credentials provided are correct");
            return false;
        }
		$sql = "SET NAMES 'UTF8'";
		$result =($this->connection)->query($sql);
        if($result <= 0)
        {
            $this->HandleDBError('Error setting utf8 encoding');
            return false;
        }
        return true;
    }    
    
    function Ensuretable()
    {
		$sql = "SHOW COLUMNS FROM $this->tablename";
        $result = ($this->connection)->query($sql);
        if(!$result || mysqli_num_rows($result) <= 0)
        {
            return $this->CreateTable();
        }
        return true;
    }
    
    function CreateTable()
    {
        $qry = "Create Table $this->tablename (".
                "id_user INT NOT NULL AUTO_INCREMENT ,".
                "name VARCHAR( 128 ) NOT NULL ,".
                "email VARCHAR( 64 ) NOT NULL ,".
                "phone_number VARCHAR( 16 ) NOT NULL ,".
                "username VARCHAR( 16 ) NOT NULL ,".
                "password VARCHAR( 32 ) NOT NULL ,".
                "confirmcode VARCHAR(32) ,".
                "PRIMARY KEY ( id_user )".
                ")";
                
        if(!mysql_query($qry,$this->connection))
        {
            $this->HandleDBError("Error creating the table \nquery was\n $qry");
            return false;
        }
        return true;
    }
	function UpdateSetting($AlertId,$value)
	{$insert_query = 'update Alerts Set
		        PeriodId ="' . $this->SanitizeForSQL($value) . '"
                Where AlertId ="' . $this->SanitizeForSQL($AlertId) . '"';
		$result = ($this->connection)->query($insert_query);
        if(!$result)
        {
            $this->HandleDBError("Error Updating data to the table\nquery:$insert_query");
            return false;
        }        
        return true;
	}
	function InsertSetting($AlertTypeId,$value,$userid)
	{
		 $insert_query = 'insert into Alerts(
		        AlertId,
                UserId,
				PeriodId,
				AlertTypeId,
				LastUpdated
				)
                values
                (
                "' . $this->SanitizeForSQL($userid . $AlertTypeId) . '",
                "' . $this->SanitizeForSQL($userid) . '",
                "' . $this->SanitizeForSQL($value) . '",
				"' . $this->SanitizeForSQL($AlertTypeId) . '",
                "' . $this->SanitizeForSQL(time()) . '"
                )';  
		$result = ($this->connection)->query($insert_query);
        if(!$result)
        {
            $this->HandleDBError("Error inserting data to the table\nquery:$insert_query");
            return false;
        }        
        return true;
	}
    function InsertSettingIntoDB(&$formvars)
	{
		$userid = $this->UserId();
		foreach ($formvars as $key => $value)
		{
		  $field_val = $this->SanitizeForSQL($key);
          $qry = "select AlertId from Alerts where UserId='$userid' and AlertTypeId='$key'";
          $result = ($this->connection)->query($qry);
          if($result && mysqli_num_rows($result) > 0)
          {
			$row = mysqli_fetch_assoc($result);
            if(!$this->UpdateSetting($row['AlertId'],$value))
			{
			  $this->HandleDBError("Error could not update alert setting");
			  return false;
			}
          }else {
		    if(!$this->InsertSetting($key,$value,$userid))
			{
				$this->HandleDBError("Error could not insert alert setting");
				return false;
			}
		  }
		}
		return true;
	}
   
    
    function InsertUserIntoDB(&$formvars)
    {
    
        //$confirmcode = $this->MakeConfirmationMd5($formvars['email']);
        
        //$formvars['confirmcode'] = $confirmcode;
        
		/*$formvars['UserID'] = $this->Sanitize($_POST['UserID']);
		$formvars['fName'] = $this->Sanitize($_POST['fName']);
        $formvars['lName'] = $this->Sanitize($_POST['lName']);
        $formvars['email'] = $this->Sanitize($_POST['email']);
        $formvars['Password'] = $this->Sanitize($_POST['Password']);*/
        $insert_query = 'insert into '.$this->tablename.'(
                UserID,
                Email,
                FirstName,
                LastName,
                Password,
		UserTypeID
                )
                values
                (
                "' . $this->SanitizeForSQL($formvars['UserID']) . '",
                "' . $this->SanitizeForSQL($formvars['email']) . '",
                "' . $this->SanitizeForSQL($formvars['fName']) . '",
				"' . $this->SanitizeForSQL($formvars['lName']) . '",
                "' . md5($formvars['Password']) . '",
				"' . $this->SanitizeForSQL(1) . '"
                )';  
		$result = ($this->connection)->query($insert_query);
        if(!$result)
        {
            $this->HandleDBError("Error inserting data to the table\nquery:$insert_query");
            return false;
        }        
        return true;
    }
    function MakeConfirmationMd5($email)
    {
        $randno1 = rand();
        $randno2 = rand();
        return md5($email.$this->rand_key.$randno1.''.$randno2);
    }
    function SanitizeForSQL($str)
    {
        if( function_exists( "mysql_real_escape_string" ) )
        {
              $ret_str = mysqli_real_escape_string( $str );
        }
        else
        {
              $ret_str = addslashes( $str );
        }
        return $ret_str;
    }
    
 /*
    Sanitize() function removes any potential threat from the
    data submitted. Prevents email injections or any other hacker attempts.
    if $remove_nl is true, newline chracters are removed from the input.
    */
    function Sanitize($str,$remove_nl=true)
    {
        $str = $this->StripSlashes($str);

        if($remove_nl)
        {
            $injections = array('/(\n+)/i',
                '/(\r+)/i',
                '/(\t+)/i',
                '/(%0A+)/i',
                '/(%0D+)/i',
                '/(%08+)/i',
                '/(%09+)/i'
                );
            $str = preg_replace($injections,'',$str);
        }

        return $str;
    }    
    function StripSlashes($str)
    {
        if(get_magic_quotes_gpc())
        {
            $str = stripslashes($str);
        }
        return $str;
    }    
}
?>
