<?PHP
require_once("./include/membersite_config.php");
if(isset($_POST['Register']))
{
   if($fgmembersite->RegisterUser())
   {
     $fgmembersite->RedirectToURL("index.php"); 
   }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Labadmin Notification System</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Register an Account</div>
      <div class="card-body">
        <form  id='register' name='registration' action='<?php echo $fgmembersite->GetSelfScript(); ?>'  method='post' accept-charset='UTF-8' onsubmit="return formValidation();">
          <div class="form-group">
            <label for="UserID">ID Number</label>
            <input class="form-control" id="UserID" name="UserID" type="text"  value='<?php echo $fgmembersite->SafeDisplay('UserID')?>' placeholder="Enter ID Number"/>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="fName">First name</label>
                <input class="form-control" id="fName" name="fName" type="text" value='<?php echo $fgmembersite->SafeDisplay('fName') ?>' maxlength="25"  aria-describedby="nameHelp" placeholder="Enter first name"/>
				<span id='register_fname_errorloc' class='error'></span>
              </div>
              <div class="col-md-6">
                <label for="lName">Last name</label>
                <input class="form-control" id="lName" name="lName" type="text" value='<?php echo $fgmembersite->SafeDisplay('lName') ?>' maxlength="25" aria-describedby="nameHelp" placeholder="Enter last name">
				<span id='register_lname_errorloc' class='error'></span>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="email">Email address</label>
            <input class="form-control" id="email" name="email" type="email" value='<?php echo $fgmembersite->SafeDisplay('email') ?>' maxlength="50" aria-describedby="emailHelp" placeholder="Enter email">
			<span id='register_email_errorloc' class='error'></span>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="Password">Password</label>
                <input class="form-control" id="Password" name="Password" type="password" placeholder="Password">
              </div>
              <div class="col-md-6">
                <label for="ConfirmPassword">Confirm password</label>
                <input class="form-control" id="ConfirmPassword" name="ConfirmPassword" type="password" placeholder="Confirm password">
              </div>
            </div>
          </div>
          <font color="red">
            <div id="error-field">
              <?php $fgmembersite->PrintError(); ?>
            </div>
          </font>
          <input class="btn btn-primary btn-block" type='submit' name='Register' value='Register'  />
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="login.html">Login Page</a>
          <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
        </div>
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <!-- input Mask-->
  <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>
  <script> $("#UserID").inputmask();</script>
  <script type="text/javascript" src="js/regFormValidation.js"></script> 
</body>

</html>
