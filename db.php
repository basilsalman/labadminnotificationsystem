<?php 
require_once 'libs/PHPMailer/PHPMailerAutoload.php';
require_once 'config.php';
function dbConnect(){
  global $DB;
  $conn = new mysqli($DB['host'], $DB['user'], $DB['pass'], $DB['dbname'],$DB['port'],$DB['sock']);
// Check connection

  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }
  return $conn;
}
function projectsTableHeader(){
 echo '<div class="table-responsive">
         <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Laboratory</th>
                  <th>Semester</th>
                  <th>Supervisor</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Laboratory</th>
                  <th>Semester</th>
                  <th>Supervisor</th>
                </tr>
              </tfoot>
              <tbody>';
}
function getAwaitingFirstMeetingProjects(&$result,$userid){
  $sql = "SELECT `project_name_eng`,`lab_name`,`semester_display_name`,`users`.first_name,`users`.last_name,`users`.tz,projects.created_on
          FROM `projects`,`labs`,`users`,`semester`
          WHERE `project_status` = 12
          AND projects.`is_active` = 1
          AND projects.id_labs = labs.id_labs
          AND is_canceled=0
          AND projects.id_semester = semester.id_semester
          AND projects.main_super_tz = users.tz
          AND users.tz = $userid"; 
  $conn = dbConnect();
  $result = $conn->query($sql);
  $conn->close();
}
function getAwaitingApprovalProjects(&$result,$userid){
  $sql = "SELECT `project_name_eng`,`lab_name`,`semester_display_name`,`users`.first_name,`users`.last_name,`users`.tz,projects.created_on
          FROM `projects`,`labs`,`users`,`semester`
          WHERE (`project_status` = 1
          OR `project_status` = 2)
          AND projects.`is_active` = 1
          AND projects.id_labs = labs.id_labs
          AND is_canceled=0
          AND projects.id_semester = semester.id_semester
          AND projects.main_super_tz = users.tz
          AND users.tz = $userid";
  $conn = dbConnect();
  $result = $conn->query($sql);
  $conn->close();
}
function getAwaitingGradingProjects($result,$userid){
  $sql = "SELECT `project_name_eng`,`lab_name`,`semester_display_name`,`users`.first_name,`users`.last_name,`users`.tz,projects.created_on
          FROM `projects`,`labs`,`users`,`semester`
          WHERE `project_status` = 260
          AND projects.`is_active` = 1
          AND projects.id_labs = labs.id_labs
          AND is_canceled=0
          AND projects.id_semester = semester.id_semester
          AND projects.main_super_tz = users.tz
          AND users.tz = $userid";
  $conn = dbConnect();
  $result = $conn->query($sql);
  $conn->close();

}
function getOpenProjects(&$result,$userid){
//		$sql = "SELECT project_name_eng,id_labs,main_super_tz,id_semester FROM `projects` where project_status = 6";
  $sql = "SELECT project_name_eng,`semester_display_name`,`users`.first_name,`users`.last_name, `labs`.lab_name
          FROM projects,`users`,`labs`,`semester`
          WHERE (project_status=20 OR project_status=100 OR project_status=10) AND projects.is_active=1
          AND is_canceled=0 AND (projects.created_by=tz OR main_super_tz=tz)
          AND projects.id_labs = labs.id_labs
          AND projects.id_semester = semester.id_semester
          AND projects.`created_on` + 3153600000 > UNIX_TIMESTAMP(now())
          AND projects.main_super_tz = $userid";
  $conn = dbConnect();
  $result = $conn->query($sql);
  $conn->close();
}
function generateProjectsTable($type, $userid){
  $result = Array();
  switch($type){
    case "open":
      getOpenProjects($result,$userid);
      break;
    case "approval":
      getAwaitingApprovalProjects($result,$userid);
      break;
    case "first_meeting":
      getAwaitingFirstMeetingProjects($result,$userid);
      break;
    case "grading":
      getAwaitingGradingProjects($result,$userid);
      break;
    default:
      return;
  }
  projectsTableHeader();
  if ($result->num_rows >0) {
    while($row = $result->fetch_assoc()) {
      echo '<tr>
            <td>' . $row["project_name_eng"]. '</td>
            <td>' . $row["lab_name"] .'</td>
            <td>' . $row["semester_display_name"] .'</td>
            <td>' . $row["first_name"] . " " . $row["last_name"] .'</td>
            </tr>';
    }
  }
  echo "</tbody>
    </table>
  </div>";
}
function getUserDetails($tz,$conn){
  $sql = "SELECT first_name,last_name,email FROM `users` WHERE tz=" . $tz ;
  $result = $conn->query($sql);
  echo $sql . "<br>";
  $first_name ="";
  $last_name = "";
  $email ="";
  if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
      $first_name = $row["first_name"];
      $last_name = $row["last_name"];
      $email = $row["email"];
      echo "Firstname: " . $row["first_name"]. " - LastName: " . $row["last_name"]. " email " . $row["email"]. "<br>";
    }
  } else {
    echo "0 results";
    return NULL;
  }
  return array('first_name' =>  $first_name,
               'last_name' => $last_name,
               'email' =>$email);
}
function sendOpenProjNotification($userDetails,$conn){
  $msg = "Hi " . $userDetails['first_name'] ." " . $userDetails['last_name'] . ".\r\n" .  "No open Projects ATM good job";
//		mail($userDetails['email'], "Open Projects Report", $msg);
  echo mail("labtest@nssl-dev.technion.ac.il", "Open Projects Report", $msg);
}
function configSmtp($mailer){
  global $SMTP;
  $mailer->IsSmtp();
  $mailer->Host = $SMTP['host'];
  $mailer->Username = $SMTP['username'];
  $mailer->Password = $SMTP['password'];
  $mailer->SMTPAuth = $SMTP['SMTPAuth'];
  $mailer->Port = $SMTP['port'];
  $mailer->SMTPSecure = $SMTP['SMTPSecure'];
}
function sendMail(){
  $mailer = new PHPMailer();
  configSmtp($mailer);
  $mailer->Subject = "TEST";
  $mailer->addAddress("salaael93@gmail.com");
  $mailer->Body = "Test";
  if(!$mailer->send()){
    echo "Mailer Error: " . $mailer->ErrorInfo;
  }else {
    echo "Message has been sent";
  }
}
//	$conn = dbConnect();
//	$userDetails = getUserDetails("001580048",$conn);
//	sendOpenProjNotification($userDetails,$conn);
//	sendMail();
//	$conn->close();
?>
